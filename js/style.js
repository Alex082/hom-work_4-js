/* 
1. Описати своїми словами навіщо потрібні функції у програмуванні.

    Они нужны для сокращения кода, позволяют разбивать сложные задачи 
    на более мелкие и простые, что снижает общую сложность программы.

2. Описати своїми словами, навіщо у функцію передавати аргумент.

    Это что-то вроде отдельной маленькой программы - если мы не 
    передадим какие либо аргументы для функцыи она соответсвенно
     не сработает потому что она не понимёт что она должна сделать.

3. Що таке оператор return та як він працює всередині функції?

    Return - это возврат, он может возвращать либо 0 тоесть не чего
    не возвращать либо возвращать то значение или значение переменной 
    которое ему задаш.
*/


// Получаем два числа и математическую операцию от пользователя через окно браузера

const firstNumber = parseFloat(+prompt("Your first number:"));
const secondNumber = parseFloat(+prompt("Your second number:"));
const operation = prompt("Your mathematical operation(+, -, *, /):");

// Функция, которая принимает два числа и операцию, и возвращает результат вычисления

function calculate(firstNumber, secondNumber, operation) {
  switch (operation) {
    case "+":
      return firstNumber + secondNumber;
    case "-":
      return firstNumber - secondNumber;
    case "*":
      return firstNumber * secondNumber;
    case "/":
      return firstNumber / secondNumber;
    default:
      return NaN;
  }
}

// Вызываем функцию и выводим результат в консоль

const result = calculate(firstNumber, secondNumber, operation);
console.log(`Результат: ${result}`);


function calculate(firstNumber, secondNumber, operation) {

  // Проверка правильности введенных данных

  while (isNaN(Number(firstNumber)) || isNaN(Number(secondNumber))) {
    console.log("You introduced incorrect data. Try again");
    firstNumber = prompt("Your first number:");
    secondNumber = prompt("Your second number:");
    operation = prompt("Your mathematical operation(+, -, *, /):");
  }
  
  // Расчет результата в зависимости от введенной операции

  let result;
  switch (operation) {
    case "+":
      result = Number(firstNumber) + Number(secondNumber);
      break;
    case "-":
      result = Number(firstNumber) - Number(secondNumber);
      break;
    case "*":
      result = Number(firstNumber) * Number(secondNumber);
      break;
    case "/":
      result = Number(firstNumber) / Number(secondNumber);
      break;
    default:
      console.log("Your operation is incorrect.");
      return;
  }

  console.log(`${firstNumber} ${operation} ${secondNumber} = ${result}`);
}